package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.BadRequestException;
import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Dtos.PacienteidDto;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Mappers.PacienteMapper;
import com.folcademy.clinica.Model.Repositories.PacienteRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service("pacienteService")
public class PacienteService {
    private final PacienteRepository pacienteRepository;
    private final PacienteMapper pacienteMapper;

    public PacienteService(PacienteRepository pacienteRepository, PacienteMapper pacienteMapper) {
        this.pacienteRepository = pacienteRepository;
        this.pacienteMapper = pacienteMapper;
    }




    //Listar pacientes
    public List<PacienteidDto> listarTodos() {
        List<Paciente> pacientes = (List<Paciente>) pacienteRepository.findAll();
        return pacientes.stream().map(pacienteMapper::entityToDto).collect(Collectors.toList());
    }

    //Listar un paciente
    public PacienteidDto listarUno(Integer id) {
        return pacienteRepository.findById(id).map(pacienteMapper::entityToDto).orElse(null);

    }

    //Crear paciente
    public PacienteidDto agregar(PacienteDto dto) {
        dto.setIdpaciente(null);
        if(dto.getNombre() == null)
            throw new BadRequestException("Por favor coloque el nombre");

        return pacienteMapper.entityToDto(pacienteRepository.save(pacienteMapper.enteroDtoToEntity(dto)));
    }

    //Editar paciente
    public PacienteDto editar(Integer idpaciente, PacienteDto dto) {
        if (!pacienteRepository.existsById(idpaciente))
            throw new NotFoundException("no existe");
//            return null;
        dto.setIdpaciente(idpaciente);
        return pacienteMapper.entityToEnteroDto(pacienteRepository.save(pacienteMapper.enteroDtoToEntity(dto)));
    }


    //Eliminar paciente
    public boolean eliminar(Integer id) {
        if (!pacienteRepository.existsById(id))
            return false;
        pacienteRepository.deleteById(id);
        return true;
    }

    //Paginacion
    public Page<PacienteidDto> listarTodosByPage(Integer pageNumber, Integer pageSize, String orderField) {
        Pageable pageable = PageRequest.of(pageNumber,pageSize, Sort.by(orderField));
        return pacienteRepository.findAll(pageable).map(pacienteMapper::entityToDto);
    }


}


