package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.BadRequestException;
import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Model.Dtos.TurnoDTO;
import com.folcademy.clinica.Model.Entities.Turno;
import com.folcademy.clinica.Model.Mappers.TurnoMapper;
import com.folcademy.clinica.Model.Repositories.TurnoRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service("turnoService")
public class TurnoService {
    private final TurnoRepository turnoRepository;
    private final TurnoMapper turnoMapper;

    public TurnoService(TurnoRepository turnoRepository, TurnoMapper turnoMapper) {
        this.turnoRepository = turnoRepository;
        this.turnoMapper = turnoMapper;
    }

    //listar Turnos

    public List<TurnoDTO> listarTodos() {
        List<Turno> turnos = (List<Turno>) turnoRepository.findAll();
        return turnos.stream().map(turnoMapper::entityToDto).collect(Collectors.toList());
    }

    //Listar un turno
    public TurnoDTO listarUno(Integer id) {
        return turnoRepository.findById(id).map(turnoMapper::entityToDto).orElse(null);

    }
    //crear Turno
    public ResponseEntity<?> crearTurno(TurnoDTO turnoDTO){
        if (turnoDTO.getFecha() == null){
            throw new BadRequestException("La fecha es obligatoria");
        }

        Turno turnoEntity = new Turno();
        turnoEntity.setFecha(turnoDTO.getFecha());
        turnoEntity.setHora(turnoDTO.getHora());
        turnoEntity.setAtendido(turnoDTO.getAtendido());
        turnoEntity.setIdpaciente(turnoDTO.getIdpaciente());
        turnoEntity.setIdmedico(turnoDTO.getIdmedico());

        turnoRepository.save(turnoEntity);
        return new ResponseEntity<>(turnoEntity, HttpStatus.OK);
    }

    //Editar turno
    public TurnoDTO editar(Integer idturno, TurnoDTO dto) {
        if (!turnoRepository.existsById(idturno))
            throw new NotFoundException("no existe");
//            return null;
        dto.setIdturno(idturno);

        return turnoMapper.entityToDto(turnoRepository.save(turnoMapper.dtoToEntity(dto)));
    }

    //Eliminar turno
    public boolean eliminar(Integer idturno) {
        if (!turnoRepository.existsById(idturno))
            return false;
        turnoRepository.deleteById(idturno);
        return true;
    }

    //Paginacion
    public Page<TurnoDTO> listarTodosByPage(Integer pageNumber, Integer pageSize, String orderField) {
        Pageable pageable = PageRequest.of(pageNumber,pageSize, Sort.by(orderField));
        return turnoRepository.findAll(pageable).map(turnoMapper::entityToDto);
    }
}


