package com.folcademy.clinica.Model.Mappers;

import com.folcademy.clinica.Model.Dtos.TurnoDTO;
import com.folcademy.clinica.Model.Entities.Turno;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class TurnoMapper {
    public TurnoDTO entityToDto(Turno entity) {
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new TurnoDTO(
                                ent.getIdturno(),
                                ent.getFecha(),
                                ent.getHora(),
                                ent.getAtendido(),
                                ent.getIdpaciente(),
                                ent.getIdmedico()
                        )
                )
                .orElse(new TurnoDTO());
    }

    public Turno dtoToEntity(TurnoDTO dto) {
        Turno entity = new Turno();
        entity.setIdturno(dto.getIdturno());
        entity.setFecha(dto.getFecha());
        entity.setHora(dto.getHora());
        entity.setIdpaciente(dto.getIdpaciente());
        entity.setIdmedico(dto.getIdmedico());
        return entity;
    }
}
