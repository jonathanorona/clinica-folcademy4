package com.folcademy.clinica.Model.Mappers;

import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Dtos.PacienteidDto;
import com.folcademy.clinica.Model.Entities.Paciente;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class PacienteMapper {

    public PacienteDto entityToEnteroDto(Paciente entity) {
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new PacienteDto(
                                ent.getId(),
                                ent.getDni(),
                                ent.getNombre(),
                                ent.getApellido(),
                                ent.getTelefono(),
                                ent.getDireccion()
                        )
                )
                .orElse(new PacienteDto());
    }

    public Paciente enteroDtoToEntity(PacienteDto dto) {
        Paciente entity = new Paciente();
        entity.setId(dto.getIdpaciente());
        entity.setDni(dto.getDni());
        entity.setNombre(dto.getNombre());
        entity.setApellido(dto.getApellido());
        entity.setTelefono(dto.getTelefono());
        entity.setDireccion(dto.getDireccion());
        return entity;
    }

    public PacienteidDto entityToDto(Paciente entity) {
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new PacienteidDto(
                                ent.getId(),
                                ent.getNombre(),
                                ent.getApellido()
                        )
                )
                .orElse(new PacienteidDto());
    }

    public Paciente dtoToEntity(PacienteidDto dto) {
        Paciente entity = new Paciente();
        entity.setId(dto.getIdpaciente());
        entity.setApellido(dto.getApellido());
        entity.setTelefono(dto.getTelefono());
        return entity;
    }
}