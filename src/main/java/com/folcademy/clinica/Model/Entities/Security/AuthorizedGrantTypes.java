package com.folcademy.clinica.Model.Entities.Security;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "authorized_grant_types")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class AuthorizedGrantTypes {
    @Column(name = "name")
    String name;
    @Column(name = "deleted", columnDefinition = "TINYINT")
    boolean deleted;
    @Column(name = "created")
    Timestamp created;
    @Column(name = "modified")
    Timestamp modified;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        AuthorizedGrantTypes that = (AuthorizedGrantTypes) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
