package com.folcademy.clinica.Model.Entities.Security;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "usuarios")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Usuario {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "usuarioid", columnDefinition = "INT(10) UNSIGNED")
    private Integer usuarioId;
    @Column(name = "usuariocodigo")
    private String usuarioCodigo;
    @Column(name = "usuariodesc")
    private String usuarioDesc;
    @Column(name = "usuarioclave")
    private String usuarioClave;
    @Column(name = "usuariohabi")
    private double usuarioHabi;
    @Column(name = "usuariodelete")
    private Integer usuarioDelete;
    @Column(name = "accesstokenvalidityseconds")
    private int accesstokenvalidityseconds;
    @Column(name = "refreshtokenvalidityseconds")
    private int refreshtokenvalidityseconds;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "usuarios_authorities",
            joinColumns = @JoinColumn(name = "usuarioid"),
            inverseJoinColumns = @JoinColumn(name = "id_authority"))
    private List<Authority> authoritySet = new ArrayList<>();

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "usuarios_scopes",
            joinColumns = @JoinColumn(name = "usuarioid"),
            inverseJoinColumns = @JoinColumn(name = "id_scope"))
    private Set<Scope> scopeSet = new HashSet<>();

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "usuarios_resources",
            joinColumns = @JoinColumn(name = "usuarioid"),
            inverseJoinColumns = @JoinColumn(name = "id_resource"))
    private Set<Resource> resourceSet = new HashSet<>();

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "usuarios_authorized_grant_types",
            joinColumns = @JoinColumn(name = "usuarioid"),
            inverseJoinColumns = @JoinColumn(name = "id_authorized_grant_types"))
    private Set<AuthorizedGrantTypes> authorizedGrantTypesSet = new HashSet<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Usuario usuario = (Usuario) o;
        return usuarioId != null && Objects.equals(usuarioId, usuario.usuarioId);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
