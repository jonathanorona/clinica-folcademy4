package com.folcademy.clinica.Model.Dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TurnoDTO {
    Integer idturno;
    LocalDate fecha;
    LocalTime hora;
    Boolean atendido;
    Integer idpaciente;
    Integer idmedico;
}