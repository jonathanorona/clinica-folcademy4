package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.TurnoDTO;
import com.folcademy.clinica.Services.TurnoService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/turnos")
public class TurnosController {

    private final TurnoService turnoService;

    public TurnosController(TurnoService turnoService) {
        this.turnoService = turnoService;
    }

    //Listar turnos
    @PreAuthorize("hasAuthority('get_turno')")
    @GetMapping("")
    public ResponseEntity<List<TurnoDTO>> listarTodo() {
        return ResponseEntity.ok(turnoService.listarTodos());
    }

    @GetMapping("/page")
    public ResponseEntity<Page<TurnoDTO>> listarTodoByPage(
            @RequestParam(name = "pageNumber",defaultValue = "1") Integer pageNumber,
            @RequestParam(name = "pageSize",defaultValue = "2") Integer pageSize,
            @RequestParam(name = "orderField",defaultValue = "fecha") String orderField
    ) {

        return ResponseEntity.ok(turnoService.listarTodosByPage(pageNumber,pageSize,orderField));
    }

    //Mostrar un turno
    @PreAuthorize("hasAuthority('get_turno')")
    @GetMapping(value = "/{idturno}")
    public ResponseEntity<TurnoDTO> listarUno(@PathVariable(name = "idturno") int id) {
        return ResponseEntity.ok(turnoService.listarUno(id));
    }

    //create turno
    @PreAuthorize("hasAuthority('post_turno')")
    @PostMapping("")
    public ResponseEntity<?> crearTurno(@RequestBody TurnoDTO turnoDTO) {
        return turnoService.crearTurno(turnoDTO);
    }

    //Editar turno
    @PreAuthorize("hasAuthority('put_turno')")
    @PutMapping("/{idturno}")
    public ResponseEntity<TurnoDTO> editar(@PathVariable(name = "idturno") int id,
                                              @RequestBody TurnoDTO dto) {
        return ResponseEntity.ok(turnoService.editar(id, dto));
    }

    //Eliminar turno
    @PreAuthorize("hasAuthority('delete_turno')")
    @DeleteMapping("/{idturno}")
    public ResponseEntity<Boolean> eliminar(@PathVariable(name = "idturno") int id) {
        return ResponseEntity.ok(turnoService.eliminar(id));
    }

}
