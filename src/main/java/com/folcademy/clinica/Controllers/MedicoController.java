package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Dtos.MedicoEnteroDto;
import com.folcademy.clinica.Services.MedicoService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/medicos")
public class MedicoController {
    private final MedicoService medicoService;

    public MedicoController(MedicoService medicoService) {
        this.medicoService = medicoService;
    }

    //Listar medicos
    @PreAuthorize("hasAuthority('get_medico')")
    @GetMapping("")
    public ResponseEntity<List<MedicoDto>> listarTodo() {
        return ResponseEntity.ok(medicoService.listarTodos());
    }



    @GetMapping("/page")
    public ResponseEntity<Page<MedicoDto>> listarTodoByPage(
            @RequestParam(name = "pageNumber",defaultValue = "1") Integer pageNumber,
            @RequestParam(name = "pageSize",defaultValue = "2") Integer pageSize,
            @RequestParam(name = "orderField",defaultValue = "apellido") String orderField
    ) {

        return ResponseEntity.ok(medicoService.listarTodosByPage(pageNumber,pageSize,orderField));
    }


    //Mostrar un medico
    @PreAuthorize("hasAuthority('get_medico')")
    @GetMapping("/{idMedico}")
    public ResponseEntity<MedicoDto> listarUno(@PathVariable(name = "idMedico") int id) {
        return ResponseEntity.ok(medicoService.listarUno(id));
    }

    //Crear medico
    @PreAuthorize("hasAuthority('post_medico')")
    @PostMapping("")
    public ResponseEntity<MedicoDto> agregar(@RequestBody @Validated MedicoEnteroDto dto) {
        return ResponseEntity.ok(medicoService.agregar(dto));
    }

    //Editar medico
    @PreAuthorize("hasAuthority('put_medico')")
    @PutMapping("/{idMedico}")
    public ResponseEntity<MedicoEnteroDto> editar(@PathVariable(name = "idMedico") int id,
                                                  @RequestBody MedicoEnteroDto dto) {
        return ResponseEntity.ok(medicoService.editar(id, dto));
    }

    //Editar consulta
    @PreAuthorize("hasAuthority('put_medico')")
    @PutMapping("/{idMedico}/consulta/{consulta}")
    public ResponseEntity<Boolean> editarConsulta(@PathVariable(name = "idMedico") int id,
                                                  @PathVariable(name = "consulta") int consulta) {
        return ResponseEntity.ok(medicoService.editarConsulta(id, consulta));
    }

    //Eliminar medico
    @PreAuthorize("hasAuthority('delete_medico')")
    @DeleteMapping("/{idMedico}")
    public ResponseEntity<Boolean> eliminar(@PathVariable(name = "idMedico") int id) {
        return ResponseEntity.ok(medicoService.eliminar(id));
    }
}
