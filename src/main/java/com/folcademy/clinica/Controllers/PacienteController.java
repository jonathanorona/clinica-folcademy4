package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Dtos.PacienteidDto;
import com.folcademy.clinica.Services.PacienteService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/pacientes")
public class PacienteController {

    private final PacienteService pacienteService;

    public PacienteController(PacienteService pacienteService) {
        this.pacienteService = pacienteService;
    }


    //Listar pacientes
    @PreAuthorize("hasAuthority('get_paciente')")
    @GetMapping("")
    public ResponseEntity<List<PacienteidDto>> listarTodo() {
        return ResponseEntity.ok(pacienteService.listarTodos());
    }

    @GetMapping("/page")
    public ResponseEntity<Page<PacienteidDto>> listarTodoByPage(
            @RequestParam(name = "pageNumber",defaultValue = "1") Integer pageNumber,
            @RequestParam(name = "pageSize",defaultValue = "2") Integer pageSize,
            @RequestParam(name = "orderField",defaultValue = "apellido") String orderField
    ) {

        return ResponseEntity.ok(pacienteService.listarTodosByPage(pageNumber,pageSize,orderField));
    }


    //Mostrar un paciente
    @PreAuthorize("hasAuthority('get_paciente')")
    @GetMapping(value = "/{idPaciente}")
    public ResponseEntity<PacienteidDto> listarUno(@PathVariable(name = "idPaciente") int id) {
        return ResponseEntity.ok(pacienteService.listarUno(id));
    }

    //Crear paciente
    @PreAuthorize("hasAuthority('post_paciente')")
    @PostMapping("")
    public ResponseEntity<PacienteidDto> agregar(@RequestBody @Validated PacienteDto dto) {
        return ResponseEntity.ok(pacienteService.agregar(dto));
    }

    //Editar paciente
    @PreAuthorize("hasAuthority('put_paciente')")
    @PutMapping("/{idPaciente}")
    public ResponseEntity<PacienteDto> editar(@PathVariable(name = "idPaciente") int id,
                                                  @RequestBody PacienteDto dto) {
        return ResponseEntity.ok(pacienteService.editar(id, dto));
    }

    //Eliminar paciente
    @PreAuthorize("hasAuthority('delete_paciente')")
    @DeleteMapping("/{idPaciente}")
    public ResponseEntity<Boolean> eliminar(@PathVariable(name = "idPaciente") int id) {
        return ResponseEntity.ok(pacienteService.eliminar(id));
    }
}



